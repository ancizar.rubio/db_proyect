USE databaseproyecto_Banco;

CREATE TABLE C_Corriente(
Id_C_Corriente int unsigned auto_increment,
No_Cuenta_Corriente int(20) not null,
Saldo_Cuenta_Corriente int not null,
PRIMARY KEY (Id_C_Corriente)
);

CREATE TABLE C_Ahorro(
Id_C_Ahorro int unsigned auto_increment,
No_Cuenta_Ahorro int(20) not null,
Saldo_Cuenta_Ahorro int not null,
PRIMARY KEY (Id_C_Ahorro)
);

CREATE TABLE C_Credito(
Id_C_Credito int unsigned auto_increment,
No_Cuenta_Credito int(20) not null,
Saldo_Cuenta_Credito int not null,
PRIMARY KEY (Id_C_Credito)
);

CREATE TABLE Tipo_Cuenta(
Id_Tipo_Cuenta int unsigned auto_increment,
Id_C_Corriente int,
PRIMARY KEY (Id_Tipo_Cuenta),
CONSTRAINT FK_C_Corriente FOREIGN KEY (Id_C_Corriente)
    REFERENCES C_Corriente(Id_C_Corriente)
);

CREATE TABLE Cliente(
Id_Cliente int unsigned auto_increment,
Nombre_Cliente varchar(255) not null,
FOREIGN KEY (Id_Tipo_Cuenta) REFERENCES Tipo_Cuenta(Id_Tipo_Cuenta),
PRIMARY KEY (Id_Cliente)
);

CREATE TABLE Banco_xyz(
Id_Banco INT UNSIGNED AUTO_INCREMENT,
FOREIGN KEY (Id_Cliente) REFERENCES Cliente(Id_Cliente),
FOREIGN KEY (Id_Tipo_Cuenta) REFERENCES Tipo_Cuenta(Id_Tipo_Cuenta),
PRIMARY KEY (Id_Banco)
);

